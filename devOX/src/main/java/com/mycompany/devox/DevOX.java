/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.devox;
import java.util.Scanner;
import static com.mycompany.devox.DevOX.row;
import static com.mycompany.devox.DevOX.col;
/**
 *
 * @author Paweena Chinasri
 */
public class DevOX {
        static char[][] table = {{'-','-','-'},
    {'-','-','-'},
    {'-','-','-'}
    };
    static char currentPlayer = 'X';
    static int row,col;
    static String ans;
    static void printWelcome(){
        System.out.println("!!!WELCOME TO OX!!!");   
    }
    static void printTable(){
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }
    static void printTurn(){
        System.out.println(currentPlayer + " Turn");
    }
    static void inputRowCol(){
        Scanner kb = new Scanner(System.in);
        while(true){
        System.out.print("Plese input row,col:");
        row = kb.nextInt();
        col = kb.nextInt();
        if(table[row-1][col-1] == '-'){
            table[row-1][col-1] = currentPlayer;
            break;
        //System.out.println(""+row + "," +col);
        }
      }
    }
    static void resetOX(){
        currentPlayer = 'X';
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
            table[i][j] = '-';
        }
      }
    }
    static void inputCon(){
        Scanner ox = new Scanner(System.in);
        System.out.println("Continue (Y/N) :");
        ans = ox.next();
        while(true){
            if(ans .equals ("n")){
                System.out.println("Bye Bye");
                break;
            }else if(ans .equals("Y")){
                break;
            }
        }
    }
    static void switchPlayer(){
        if(currentPlayer == 'X'){
            currentPlayer = 'O';
        }else{
            currentPlayer = 'X';
        }
      }
    //check
    static boolean checkRow(){
        for(int i=0; i<3 ;i++){
            if(table[row-1][i] != currentPlayer){
                return false;
            }
        }
        return true;
    }
     static boolean checkCol(){
        for(int i=0; i<3 ;i++){
            if(table[i][col-1] != currentPlayer){
                return false;
            }
        }
        return true;
     }
      static boolean checkDiago1(){
            if(table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer){
                return true;
            }
        
        return false;
     }
     static boolean checkDiago2(){
            if(table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer){
                return true;
            }
        
        return false;
     }
     static boolean isWin(){
         while(true){
            if(checkRow()){
                return true;
        }
            if(checkCol()){
                return true;
        }
            if(checkDiago1()){
                return true;
        }
            if(checkDiago2()){
                return true;
        }   
            return false;
         }
     }
    static boolean isDraw(){
        for(int i = 0; i<3; i++){
            for(int j = 0; j<3; j++){
                if(table[i][j] == '-'){
                    return false;
                }
            }
        }
        return true;
      }
    
       static void printWin(){
        System.out.println(currentPlayer + " Win!!!");
    }
       static void printDraw(){
        System.out.println("Draw!!!");
       }
       
    public static void main(String[] args){
        printWelcome();
    while(true){
        while(true){
            printTable();
            printTurn();
            inputRowCol();
            if(isWin()){
                printTable();
                printWin();
                break;
            }else if(isDraw()){
                printTable();
                printDraw();
                break;
        }
        switchPlayer();
    }
        inputCon();
        if(ans .equals("N")){
            break;
        }
        resetOX();
    }
  }
}
